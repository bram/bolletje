// beschrijving: holle proton met magneetjes en klittenband.
// kleur: rood
// vulling: 22 mm knikker
//
// Wordt gevuld met knikker voor extra gewicht
// magneetjes hebben een diameter van 10mm en zijn 1mm dik
// Voor The Associator MVP-003 atomen 11-2023
include <BOSL/constants.scad>
use <BOSL/transforms.scad>
use <functions.scad>

// diameter bol
d = 32;
r = d / 2;

// maat knikker als vulling
knikkerdia = 22.5;

// maten magneet
magneetdia = 10.4;
magneetverzonken = 1.9; // hiermee komt de rand van de magneet aan de boloppervlakte te zitten

// maten klittenband rondjes
klittenbanddia=10;
klittenbandverzonken=1;

angleoffset=20; // 20 goed wanneer klittenband aanwezig is

// detailniveau
$fn=100;

// in tweeen splitsen om te kunnen printen
translate( [0, 0, 1] )
    top_half()
    bolletje();

translate( [0, 0, -1] )
    bottom_half()
    difference() {
        bolletje();
       rotate([90,  0, -58]) rhombus( h = 2, w = 1, d = r);
    }

module magneetgat() {
    translate([0, 0, r - magneetverzonken])
        cylinder(d = magneetdia, h = 1.2 * magneetverzonken);
}

module vulgat() {
    cylinder( d = magneetdia - 2, h = 1.1 * r );
}

module klittenbandgat() {
    translate([0,0,r-klittenbandverzonken])
        cylinder(d=klittenbanddia, h=1.1*klittenbandverzonken);
}

module bolletje( $fn = 100) {
    bolmassa(r = knikkerdia / 2000, soortelijke_massa = 2600);

    difference() {
        color( "red" ) sphere(r = r);
        sphere( d = knikkerdia ); // holte
        // extra uitsparing om nabewerking te voorkomen
        translate([0,0,.25*knikkerdia])sphere(d=.7*knikkerdia);
        translate([0,0,-.25*knikkerdia])sphere(d=.7*knikkerdia);

        // markeringen
        rotate([90,  0, -50]) rhombus( h = 2, w = 1, d = r); // printvriendelijke ruit die in beide helften zit
        rotate( [angleoffset, angleoffset, 0] ) { // hiermee draai je het hele stelsel in een keer
            rotate([  90,   0, 0]) magneetgat();
            rotate([ -90,   0, 0]) magneetgat();

            rotate([   0,  90, 0]) magneetgat();
            rotate([   0, -90, 0]) magneetgat();

            rotate([   0,   0, 0]) magneetgat();
            rotate([ 180,   0, 0]) magneetgat();

            zflip_copy() { // flip maar in het xy-vlak, dan krijgen we daar de andere 4 klittenbandgaatjes vanzelf
                rotate([+135, 0, 0]) klittenbandgat();
                rotate([-135, 0, 0]) klittenbandgat();
                rotate([0, 135, 0]) klittenbandgat();
                rotate([0, -135, 0]) klittenbandgat();
            }

            rotate([90,  0, 45]) klittenbandgat();
            rotate([-90, 0, 45]) klittenbandgat();

            rotate([90,  0, -45]) klittenbandgat();
            rotate([-90, 0, -45]) klittenbandgat();
        }
    }
}
