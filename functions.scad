// functions

module bolmassa (r=1, soortelijke_massa=1000) {
    
    volume=PI*r^3*4/3;
    echo ("volume: ", volume, "m^3");
    massa=soortelijke_massa*volume;
    echo ("massa: ", massa, "kg");
    echo ("massa: ", massa*1000, "g");
}

module rhombus(h=1,w=2, d=10) {
   a = [[w/2,0], [0,h/2], [-w/2,0], [0, -h/2]];
   linear_extrude(height = d) #polygon(a);
}