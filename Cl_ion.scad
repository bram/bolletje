// beschrijving: holle Cl ion met magneetjes. Hoeft niet gevuld te worden.
// kleur: groen
// vulling: geen
//
// magneetjes hebben een diameter van 5mm en zijn 1mm dik.abs
// Voor The Associator MVP-002 zout 11-2023
include <BOSL/constants.scad>
use <BOSL/transforms.scad>
use <functions.scad>

// diameter bol
d = 28;
r = d / 2;

// maten magneet
magneetdia = 5.4;
magneetverzonken = 1.2; // hiermee komt de rand van de magneet aan de boloppervlakte te zitten
magneetondersteuning = 0.8; // hoeveel materiaal moet er onder de magneet zitten. als dit voldoende klein is, valt het magneetje in de bol ;-)

angleoffset = 30; // 30 best voor als geen klittenband

// detailniveau
$fn=100;

// in tweeen splitsen om te kunnen printen
translate( [0, 0, 1] )
top_half() 
bolletje();

translate( [0, 0, -1] )
bottom_half()
bolletje();


module magneetgat() {
    translate([0, 0, r - magneetverzonken])
        cylinder(d = magneetdia, h = 1.2 * magneetverzonken);
}

module bolletje( $fn = 100) {
    ri = r - magneetverzonken - magneetondersteuning;
  
    difference() {
        color( "green" ) sphere(r = r);
        sphere( r = ri ); // holte
        
        // markeringen
        rotate([90,  0, -16.5])     
            rhombus( h = 1, w = 1, d = r); //printvriendelijke ruit die in beide helften zit
        rotate( [angleoffset, angleoffset, 0] ) { // hiermee draai je het hele stelsel in een keer
            rotate([  90,   0, 0]) magneetgat();
            rotate([ -90,   0, 0]) magneetgat();

            rotate([   0,  90, 0]) magneetgat();
            rotate([   0, -90, 0]) magneetgat();

            rotate([   0,   0, 0]) magneetgat();
            rotate([ 180,   0, 0]) magneetgat();
        }
    }
}
