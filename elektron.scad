// beschrijving: elektron met magneetjes. Hoeft niet gevuld te worden.
// kleur: zwart
// vulling: geen
//
// magneetjes hebben een diameter van 3mm en zijn 1mm dik
// Voor The Associator MVP-003 atomen 11-2023
include <BOSL/constants.scad>
use <BOSL/transforms.scad>
use <functions.scad>

// diameter bol
d = 8;
r = d / 2;

// maten magneet
magneetdia = 3.4;
magneetverzonken = 1.2; // hiermee komt de rand van de magneet aan de boloppervlakte te zitten
magneetondersteuning = 0.8; // hoeveel materiaal moet er onder de magneet zitten. als dit voldoende klein is, valt het magneetje in de bol ;-)

// detailniveau
$fn=100;

// bolletje, niet twee helften
rotate([180,0,0])bolletje();

module magneetgat() {
    translate([0, 0, r - magneetverzonken])
        cylinder(d = magneetdia, h = 1.2 * magneetverzonken);
}

module steuntje() {
    translate([0, 0, r - magneetverzonken])
        difference() {
            cylinder(d = magneetdia + .8, h = 1.5);
            translate([0,0,-.1])cylinder(d = magneetdia , h = 2.2);
        }
}

module bolletje( $fn = 100) {
    ri = r - magneetverzonken - magneetondersteuning;
  
    difference() {
        color( "black" ) sphere(r = r);
        sphere( r = ri ); // holte
        
        magneetgat();
    }
    translate([0,0,.5])steuntje();
}
